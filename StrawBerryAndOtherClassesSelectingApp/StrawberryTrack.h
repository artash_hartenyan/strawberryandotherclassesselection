#ifndef STRAWBERRYTRACK_H
#define STRAWBERRYTRACK_H

#include <vector>

#include <opencv2\core\utility.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\tracking\tracking.hpp>

class StrawberryTracker {


private:
	cv::MultiTracker* m_strawMultiTracker;
	std::string m_trackerName;
	cv::Mat m_currentImg;
	std::vector<cv::Rect2d> m_trackerObjRects;
	std::vector<bool> m_isTrackedRectValid;
	std::vector<bool> m_isTrackedRectAddedToCount;

public:
	StrawberryTracker(const std::string& = "MEDIANFLOW", const cv::Mat& = cv::Mat() );
	void addNewStrawToTrack(const cv::Rect2d& newRect);
	void changeCurrentRectValidity(const unsigned int& indexOfRect, const bool& validity);
	void changeCurrentRectStateForCounted(const unsigned int& indexOfRect, const bool& stateForCounted);
	void setCurrentImage(const cv::Mat& img);
	void removeStrawFromTrack(const std::vector<cv::Rect2d>& remRectVec);
	void updateTrackerInfo(const cv::Mat& updImg);
	std::vector<cv::Rect2d> getTrackedStrawberries();
	std::vector<cv::Rect2d> getRectsFromTrackVector();
	bool getTrackerCurrentRectValidityByIndex(const unsigned int& index) const;
	bool getTrackerCurrentRectStateForCounted(const unsigned int& index) const;
	cv::Rect2d getTrackerCurrentRectByIndex(const unsigned int& index);
	~StrawberryTracker();

};


#endif
