#ifndef DialogInfoRemove_H
#define DialogInfoRemove_H

#include <QDialog>
#include <QMenu>
#include <QMenuBar>

typedef std::pair<std::string, std::vector<std::string>> classAndSubClassesType;
class QLineEdit;
class QPushButton;

class DialogInfoRemove : public QDialog
{
	Q_OBJECT

public:
	DialogInfoRemove(QWidget *parent = 0);
	void addItemsToMenu(const std::vector<QMenu*>& dataToAdd);
	void setCurrentItemsInMenu(const std::vector<QMenu*>& dataToAdd);
	std::vector<QMenu*> getCurrentItemsInMenu();
//	void clearVectorInfo();
//	void clearContent();
//	void setStartValue(bool);

//	public slots:
//	void onButtonCklicked();
//	void onButtonSaveClicked();
private:
	void createMembers();
//	void setupLayout();
//	void makeConnections();

signals:
	void onRemoveClassName(const QString& className);
//	void onVectorElements(classAndSubClassesType);
public slots:
	void onSelectRemovedClass(QAction*);


private:
	QMenuBar* m_classMenuBar;
	QMenu* m_classMenu;
	QLineEdit* m_className;
	std::vector<QMenu*> m_currentItemsInMenu;
	/*QLineEdit* m_subName;
	QPushButton* m_add;
	QPushButton* m_saveCurrentClass;
	classAndSubClassesType m_classWithSubClassesVec;
	bool isstart;*/


};

#endif 
