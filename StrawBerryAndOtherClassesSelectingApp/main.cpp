#include "VideoShowDemo.h"
#include <QApplication>
#include <QTableWidget>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QTableView>
#include <QObject>
#include <QDebug>
#include <iostream>


/*New added */
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgcodecs\imgcodecs.hpp>
#include <opencv2\imgproc\imgproc.hpp>
/**/



bool  isRectsAreIntersected(const cv::Rect& rect1, const cv::Rect& rect2)
{
	return ( (rect1.y >= rect2.y && (rect1.y - rect2.y) <= rect2.height+10) || (rect2.y >= rect1.y && (rect2.y - rect1.y) <= rect1.height+10)) && 
	     	((rect1.x >= rect2.x && (rect1.x - rect2.x) <= rect2.width+10) || (rect2.x >= rect1.x && (rect2.x - rect1.x) <= rect1.width+10));
}

int main(int argc, char *argv[])
{

	

/*	QApplication a(argc, argv);
	QTableView *view = new QTableView;
	QStandardItemModel *mod = new QStandardItemModel;
	QStandardItem *it = new QStandardItem(QObject::tr("ClassName"));
	mod->setHorizontalHeaderItem(0, it);
//	QStandardItem *it1 = new QStandardItem(QObject::tr("SublassName"));
	QStandardItem *it11 = new QStandardItem(QObject::tr("Name1"));
	it->appendRow(it11);
	//mod->setHorizontalHeaderItem(1, it1);
	//QStandardItem *it2 = new QStandardItem(QObject::tr("City"));
	//mod->setHorizontalHeaderItem(2, it2);
	//QStandardItem *it3 = new QStandardItem(QObject::tr("Country"));
	//mod->setHorizontalHeaderItem(3, it3);
	//mod->setVerticalHeaderItem(2, it11);




	//and so on
	view->setModel(mod);
	view->show();
	return a.exec();*/
	
   QApplication a(argc, argv);  
    VideoShowDemo w;
    w.show();
  return a.exec();
}


void filterAndIntersectAllPossibleRects(const std::vector<cv::Rect>& initRectsList, std::vector<cv::Rect>& outputFilteredRect)
{
	bool existingRectForJoin = true;
	std::vector<cv::Rect> tmpRectsVec = initRectsList;
	while (existingRectForJoin){
		for (int i = 0; i < tmpRectsVec.size(); ++i) {
			for (int j = 0; j < tmpRectsVec.size(); ++j) {
				//cv::Rect intersectedRect = tmpRectsVec[i] & tmpRectsVec[j];
				if (i != j &&  isRectsAreIntersected(tmpRectsVec[i], tmpRectsVec[j])) {
					cv::Rect newRect;
//					isRectsAreIntersected(filtredBoundRect[i], filtredBoundRect[j]) && j != i ) {
					newRect.x = std::min(tmpRectsVec[i].x, tmpRectsVec[j].x);
					newRect.y = std::min(tmpRectsVec[i].y, tmpRectsVec[j].y);
					newRect.width = std::max(tmpRectsVec[i].width + tmpRectsVec[i].x, tmpRectsVec[j].width + tmpRectsVec[j].x) - std::min(tmpRectsVec[i].x, tmpRectsVec[j].x);
					newRect.height = std::max(tmpRectsVec[i].height + tmpRectsVec[i].y, tmpRectsVec[j].height + tmpRectsVec[j].y) - std::min(tmpRectsVec[i].y, tmpRectsVec[j].y);

					std::cout << "Initi loop " << std::endl;
					tmpRectsVec[i] = newRect;
					std::cout << "Vector size before " << tmpRectsVec.size() << std::endl;
					tmpRectsVec.erase(tmpRectsVec.begin() + j);
					std::cout << "Vector size after " << tmpRectsVec.size() << std::endl;
					existingRectForJoin = true;
					i = tmpRectsVec.size() + 2;
					j = tmpRectsVec.size() + 2;
					//existingRectForJoin = false;
		
					break;
				}
				if (i == tmpRectsVec.size() - 1 && j == tmpRectsVec.size() - 1) {
					std::cout << "Fininshing loop " << std::endl;
					i = tmpRectsVec.size() + 10;
					j = tmpRectsVec.size() + 10;
					existingRectForJoin = false;
				}
			}
		}
		
	}
	outputFilteredRect = tmpRectsVec;
}


