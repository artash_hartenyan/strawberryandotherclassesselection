#include "DialogInfoi.h"

#include <QLineEdit>
#include <QPushButton>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QMessageBox>

#include <iostream>


DialogInfoi::DialogInfoi(QWidget *parent)
    : QDialog(parent)
{
    createMembers();
    setupLayout();
    makeConnections();

    isstart = false;
}

void DialogInfoi::createMembers()
{
    m_className = new QLineEdit(this);
    m_subName= new QLineEdit(this);
    m_add = new QPushButton("add",this);
	m_saveCurrentClass = new QPushButton("save", this);
	m_saveCurrentClass->setEnabled(false);
    m_add->setFixedWidth(40);
	m_saveCurrentClass->setFixedWidth(100);
    m_className->setPlaceholderText("class name");
    m_subName->setPlaceholderText("sub-class name");
	//m_classWithSubClassesVec.clear();

}

void DialogInfoi::setupLayout()
{
	QVBoxLayout* vLay = new QVBoxLayout;
    QHBoxLayout* hlay = new QHBoxLayout;

    hlay->addWidget(m_className);
    hlay->addWidget(m_subName);
    hlay->addWidget(m_add);

    hlay->setSpacing(5);
    hlay->setContentsMargins(50,50,50,50);
	vLay->addLayout(hlay);
	QHBoxLayout* hLatSaveBut = new QHBoxLayout;
	hLatSaveBut->setSpacing(30);
	hLatSaveBut->addWidget(m_saveCurrentClass);
	vLay->addLayout(hLatSaveBut);
    setLayout(vLay);
}

void DialogInfoi::makeConnections()
{
 connect(m_add,SIGNAL(clicked()),this,SLOT(onButtonCklicked()));
 connect(m_saveCurrentClass, SIGNAL(clicked()), this, SLOT(onButtonSaveClicked()));
}


void DialogInfoi::clearVectorInfo()
{
	m_classWithSubClassesVec.first = "";
	m_classWithSubClassesVec.second.clear();
}
void DialogInfoi::clearContent()
{
	m_saveCurrentClass->setEnabled(false);
	m_className->setEnabled(true);
	//m_className->setPlaceholderText("class name");
	m_className->setText("class name ");
	
	m_subName->setPlaceholderText("sub-class name");
}

void DialogInfoi::setStartValue(bool startValue)
{
	isstart = startValue;
}

void DialogInfoi::onButtonCklicked()
{
    if(isstart){
        if(m_subName->text().isEmpty()){
             QMessageBox::warning(this, tr("My Application"),
                                           tr("Sub name empty"),
                                           QMessageBox::Ok);


        }else{
            emit(m_className->text(),m_subName->text());
			m_classWithSubClassesVec.second.push_back(m_subName->text().toStdString());
            m_subName->clear();
        }
    }else{
        if(m_className->text().isEmpty()){
             QMessageBox::warning(this, tr("My Application"),
                                           tr("Class name empty"),
                                           QMessageBox::Ok );
        }else if(m_subName->text().isEmpty()){
             QMessageBox::warning(this, tr("My Application"),
                                           tr("Sub name empty"),
                                           QMessageBox::Ok);
        }else{
            isstart = true;
            m_className->setEnabled(false);
            emit(m_className->text(),m_subName->text());
			m_classWithSubClassesVec.first = m_className->text().toStdString();
			m_classWithSubClassesVec.second.push_back(m_subName->text().toStdString());
			m_saveCurrentClass->setEnabled(true);
            m_subName->clear();
			


        }
    }
}

void DialogInfoi::onButtonSaveClicked()
{
	std::cout << "push sAVE BUTTON " << std::endl;
	isstart = false;
	emit onVectorElements(m_classWithSubClassesVec);
}
