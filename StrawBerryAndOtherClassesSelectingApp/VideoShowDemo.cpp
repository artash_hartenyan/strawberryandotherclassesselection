

#include "VideoShowDemo.h"


#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>

#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <QPushButton>
#include <QGridLayout>
#include <QSpinBox>
#include <QList>
#include <QTextLayout>
#include <QLabel>
#include <QDir>
#include <QDebug>
#include <QTimer>
#include <QString>
#include <QFileDialog>
#include <QSlider>
#include <QTableWidget>
#include <QRadioButton>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>



VideoShowDemo::VideoShowDemo(QWidget *parent) :
	QWidget(parent)

{
	createMembers();
	setupLayout();
	makeConnections();
	resize(1000, 600);
}

void  VideoShowDemo::createMembers()
{
	//m_optionWidget = new OptionWidget();
	//	m_textRegionExtractor = new TextRegionExtractor;
	m_graphicsViewVideo = new QGraphicsView;
	//m_graphicsViewText = new QGraphicsView;
	m_currentRectItem = new QGraphicsRectItem();
	m_scenvideo = new QGraphicsScene;
	//	m_scen_text = new QGraphicsScene;
	m_timer = new QTimer;
	m_pushButtonStart_Stop = new QPushButton("START");
	m_pushButtonOpenVideo = new  QPushButton("Open video");
	m_pushButtonOpenVideo->setFixedSize(100, 30);

	m_pushButtonSaveCurrentFrame = new QPushButton("Save Image");
	m_pushButtonSaveCurrentFrame->setFixedSize(100, 30);


	m_pushButtonAddClassAndSub = new QPushButton("Add class");
	m_pushButtonAddClassAndSub->setFixedSize(100, 30);

	m_pushButtonRemoveClassAndSub = new QPushButton("Remove class");
	m_pushButtonRemoveClassAndSub->setFixedSize(100, 30);



	m_videoSpeedSpinBox = new QSpinBox;
	m_videoSpeedSpinBox->setFixedSize(100, 30);
	m_videoSpeedSpinBox->setMinimum(30);
	m_videoSpeedSpinBox->setSingleStep(10);
	m_videoSpeedSpinBox->setMaximum(400);
	m_videoSpeedSpinBox->setValue(100);
	//std::cout << "Before initialize " << std::endl;
	//m_redStrawDetector = RedStrawDetector();

	m_interThreshSpinBox = new QSpinBox;
	m_interThreshSpinBox->setFixedSize(100, 30);
	m_interThreshSpinBox->setMinimum(10);
	m_interThreshSpinBox->setSingleStep(5);
	m_interThreshSpinBox->setMaximum(100);
	m_interThreshSpinBox->setValue(40);

	m_cropRectWidthSpinBox = new QSpinBox;
	m_cropRectWidthSpinBox->setFixedSize(100, 30);
	m_cropRectWidthSpinBox->setMinimum(10);
	m_cropRectWidthSpinBox->setSingleStep(5);
	m_cropRectWidthSpinBox->setMaximum(100);
	m_cropRectWidthSpinBox->setValue(50);

	m_cropRectHeightSpinBox = new QSpinBox;
	m_cropRectHeightSpinBox->setFixedSize(100, 30);
	m_cropRectHeightSpinBox->setMinimum(10);
	m_cropRectHeightSpinBox->setSingleStep(5);
	m_cropRectHeightSpinBox->setMaximum(100);
	m_cropRectHeightSpinBox->setValue(50);


	m_minimumSizeForStrawRectSpinBox = new QSpinBox;
	m_minimumSizeForStrawRectSpinBox->setFixedSize(100, 30);
	m_minimumSizeForStrawRectSpinBox->setMinimum(7);
	m_minimumSizeForStrawRectSpinBox->setSingleStep(5);
	m_minimumSizeForStrawRectSpinBox->setMaximum(100);
	m_minimumSizeForStrawRectSpinBox->setValue(10);



	m_spinTextLabel = new QLabel("Video Speed");
	m_spinTextLabel->setFixedSize(100, 30);

	m_cropRectWidthLabel = new QLabel("Crop Rect Width");
	m_cropRectWidthLabel->setFixedSize(100, 30);

	m_cropRectHeightLabel = new QLabel("Crop Rect Heigth");
	m_cropRectHeightLabel->setFixedSize(100, 30);

	m_spinInterThreshLabel = new QLabel("Intersection percent");
	m_spinInterThreshLabel->setFixedSize(100, 30);

	m_minimumSizeForStrawRectLabel = new QLabel("Minimum Straw Rect Size");
	m_minimumSizeForStrawRectLabel->setFixedSize(100, 30);

	m_countOfRaspLabel = new QLabel("Count Of Straw 0");
	m_countOfRaspLabel->setFixedSize(150, 30);
	QFont font = m_countOfRaspLabel->font();
	font.setPointSize(10);
	font.setBold(true);
	m_countOfRaspLabel->setFont(font);
	/*Setting of color for label*/
	QPalette* palette1 = new QPalette;
	//palette1->setColor(m_countOfRaspLabel->backgroundRole(), Qt::green);
	palette1->setColor(m_countOfRaspLabel->foregroundRole(), Qt::green);
	m_countOfRaspLabel->setPalette(*palette1);

	//	m_optionWidget->setFixedHeight(150);
	//	m_graphicsViewText->setStyleSheet("background-color: white;");
	//m_optionWidget->show();
	m_drawRect = false;
	m_currentFrameIndex = 0;
	m_indexOfFrameStartedAlg = 0;
	m_countOfAllRedRasp = 0;
	m_isStartedDetectionAlgorithm = false;
	m_totalCountOfRedStrawberries = 0;
	m_selectedRectsCountOnCurrentFrame = 0;
	m_intersectionAreaPercent = m_interThreshSpinBox->value();
	m_minimumStrawSize = m_minimumSizeForStrawRectSpinBox->value();
	m_indexOfCurrentSavedImage = 0;

	m_menuBar = new QMenuBar(this);
	//QMenu *fileMenu = new QMenu("Select Class");
	//menuBar->addMenu(fileMenu);
	
	/*fileMenu->addAction("StrawBerry");
	
	fileMenu->addAction("Runner");
	fileMenu->addAction("Weed");
	fileMenu->addAction("Flower");
	*/

	m_xmenu = new QMenu("Select Class");
	m_menuBar->addMenu(m_xmenu);
	/*QMenu* submenuA;
	for (int i = 0; i < 4; ++i) {
		if (i == 0) {
			submenuA = m_xmenu->addMenu("StrawBerry");
		}
		else if (i ==1) {
			submenuA = m_xmenu->addMenu("Runner");
		}
		else  if (i == 2){
			submenuA = m_xmenu->addMenu("Flower");
		}
		else {
			submenuA = m_xmenu->addMenu("Weed");
		}
		m_listOfExistingMenuItems.push_back(submenuA);
	}*/
	/*QMenu* submenuB = m_xmenu->addMenu("Runner");
	QMenu* submenuC = m_xmenu->addMenu("Flower");
	QMenu* submenuD = m_xmenu->addMenu("Weed");
	*/

	QAction* actionA_Setup1;//  = submenuA->addAction("Red");
	QAction* actionA_Setup2;// = submenuA->addAction("Green");
	QAction* actionA_Setup3;// = submenuA->addAction("Yellow");

	std::vector<std::pair<std::string, std::vector<std::string>>> lineVecOfClassesAndSubclasses;
	std::ifstream classDataInfoFile("infoDataClasses.txt");
	if (!classDataInfoFile.good())
	{
		std::cout << "Text file opened " << std::endl;
			std::ofstream myfile;
			myfile.open("infoDataClasses.txt");
			myfile<< "StrawBerry Red Green Yellow" << "\n";
			myfile << "Runned RunnerType" << "\n";
			myfile << "Flower FlowerType" << "\n";
			myfile << "Weed WeedType" << "\n";
			myfile.close();
				
	}
	classDataInfoFile.close();
	classDataInfoFile.open("infoDataClasses.txt");
	
	std::cout << "Before opening file " << std::endl;
	//classDataInfoFile.open("infoDataClasses.txt");
	std::cout << "aftr opening file " << std::endl;
	if (classDataInfoFile.is_open() )
	{
		std::cout << "Add class name before!! " << std::endl;
		std::string currentLine;
		while (std::getline(classDataInfoFile,currentLine))
		{
		//	std::vector<std::string> tmpLineInfo;
		//	splitStringIntoParts(currentLine, tmpLineInfo, ' ');
			std::istringstream iss(currentLine);
			std::vector<std::string> tmpLineInfo{ std::istream_iterator<std::string>{iss},
				std::istream_iterator<std::string>{} };
			//std::cout << "first line comp " << tmpLineInfo.size() << std::endl;
			//std::cout << "Add class name " << tmpLineInfo[0] << std::endl;
			QMenu* submenu = m_xmenu->addMenu(tmpLineInfo[0].c_str());
			
			QAction* tmpAction;
			for (int y = 1; y < tmpLineInfo.size(); ++y) {
				tmpAction = submenu->addAction(tmpLineInfo[y].c_str());
			}
			m_listOfExistingMenuItems.push_back(submenu);
			std::cout << "CUrrent manu  name " << m_listOfExistingMenuItems[0]->title().toStdString() << std::endl;
		}
		classDataInfoFile.close();
	}
	

	/*for (int i = 0; i < 4; ++i) {
		QMenu* tmpMenuItem = m_listOfExistingMenuItems[i];
		QAction* tmpAction;
		if (i == 0) {
			tmpAction = tmpMenuItem->addAction("Red");
			tmpAction = tmpMenuItem->addAction("Green");
			tmpAction = tmpMenuItem->addAction("Yellow");
		}
		else if (i == 1) {
			tmpAction = tmpMenuItem->addAction("RunnerType");
		}
		else if (i == 2) {
			tmpAction = tmpMenuItem->addAction("FlowerType");
		}
		else {
			tmpAction = tmpMenuItem->addAction("WeedType");
		}
	}*/
//	submenuA = m_xmenu->addMenu("Runner");
//	actionA_Setup1 = submenuA->addAction("RunnerType");


	/*QAction* actionB_Setup = submenuB->addAction("RunnerType");
	QAction* actionC_Setup = submenuC->addAction("FlowerType");
	QAction* actionD_Setup = submenuD->addAction("WeedType");
	*/
	m_selectedClassNameFor = new QLabel("Select class!!!");
	m_selectedClassNameFor->setFixedSize(170, 30);
	QFont font1 = m_selectedClassNameFor->font();
	font1.setPointSize(9);
	font1.setBold(true);
	m_selectedClassNameFor->setFont(font1);

	if (!QDir("cropedImageData").exists()) {
		QDir().mkdir("cropedImageData");

	}

	m_dialogInfo = new DialogInfoi();
	m_dialogRemClassInfo = new DialogInfoRemove();
	/*QString path("cropedImageData/StrawBerry");
	if (!QDir(path).exists()) {
		QDir().mkdir("cropedImageData/StrawBerry");
	}
	if (!QDir("cropedImageData/StrawBerry/Red").exists()) {
		QDir().mkdir("cropedImageData/StrawBerry/Red");
	}
	if (!QDir("cropedImageData/StrawBerry/Green").exists()) {
		QDir().mkdir("cropedImageData/StrawBerry/Red");
	}
	if (!QDir("cropedImageData/StrawBerry/Red").exists()) {
		QDir().mkdir("cropedImageData/StrawBerry/Red");
	}*/
	for (int i  = 0; i < m_listOfExistingMenuItems.size(); ++i) {
		std::cout << "before signal call !!! " << std::endl;
    	QMenu* tmpMenuItemSig = m_listOfExistingMenuItems[i];
		//if (tmpMenuItemSig-> ) {
			connect(tmpMenuItemSig, SIGNAL(triggered(QAction*)), SLOT(onRecentOpenFilesStrawBerry(QAction*)));
		//	connect(tmpMenuItemSig, SIGNAL(rightClick()), SLOT(onRecentOpenFilesStrawBerry(QAction*)));
		//}
	}
	//connect(submenuA, SIGNAL(triggered(QAction*))
	//	, SLOT(onRecentOpenFilesStrawBerry(QAction*)));
	/*connect(submenuB, SIGNAL(triggered(QAction*))
		, SLOT(onRecentOpenFilesRunner(QAction*)));
	connect(submenuC, SIGNAL(triggered(QAction*))
		, SLOT(onRecentOpenFilesFlower(QAction*)));
	connect(submenuD, SIGNAL(triggered(QAction*))
		, SLOT(onRecentOpenFilesWeed(QAction*)));
		*/


	

}

void  VideoShowDemo::setupLayout()
{
	QGridLayout * glayout = new QGridLayout(this);
	QHBoxLayout * hlayout = new QHBoxLayout();
	QHBoxLayout * spinLayout = new QHBoxLayout();

	hlayout->setGeometry(QRect(20, 20, 300, 30));
	hlayout->setSpacing(10);
	hlayout->setMargin(10);
	hlayout->setContentsMargins(-1, -1, -1, 0);
	hlayout->addWidget(m_pushButtonOpenVideo);
	hlayout->addWidget(m_pushButtonSaveCurrentFrame);
	hlayout->addWidget(m_pushButtonAddClassAndSub);
	hlayout->addWidget(m_pushButtonRemoveClassAndSub);

	spinLayout->setSpacing(5);
	spinLayout->addWidget(m_spinTextLabel, 5);
	spinLayout->addWidget(m_videoSpeedSpinBox);
	spinLayout->addWidget(m_cropRectWidthLabel);
	spinLayout->addWidget(m_cropRectWidthSpinBox);
	spinLayout->addWidget(m_cropRectHeightLabel);
	spinLayout->addWidget(m_cropRectHeightSpinBox);
	//spinLayout->addWidget(m_spinInterThreshLabel, 5);
	//spinLayout->addWidget(m_interThreshSpinBox);
	//spinLayout->addWidget(m_minimumSizeForStrawRectLabel);
	//spinLayout->addWidget(m_minimumSizeForStrawRectSpinBox);
	//spinLayout->addWidget(m_countOfRaspLabel);

	hlayout->addLayout(spinLayout);
	//vlayout->addLayout(hlayout);
	//vlayout->addWidget(m_pushButtonSaveCurrentFrame);
	glayout->addLayout(hlayout, 0, 2, 1, 1);
	//	glayout->addWidget(m_pushButtonOpenVideo, 0, 2, 1, 1);
	//	glayout->addWidget(m_spinTextLabel, 0, 3, 2, 2);
	glayout->addWidget(m_selectedClassNameFor);
	
	//	glayout->addWidget(m_videoSpeedSpinBox, 0, 4, 2, 2);
	//	glayout->addWidget(m_pushButtonSaveCurrentFrame,1,2,1,1);
	glayout->addWidget(m_graphicsViewVideo, 2, 2, 1, 1);
	//glayout->addWidget(m_graphicsViewText, 1, 3, 1, 1);
	glayout->addWidget(m_pushButtonStart_Stop, 3, 2, 1, 1);
	//glayout->addWidget(m_optionWidget,3,2,1,3);
	m_graphicsViewVideo->setScene(m_scenvideo);
	m_scenvideo->installEventFilter(this);

	//	m_graphicsViewText->setScene(m_scen_text);
	//	m_graphicsViewText->resize(m_graphicsViewVideo->size());
	setLayout(glayout);
}

void  VideoShowDemo::makeConnections()
{

	connect(m_timer, SIGNAL(timeout()), this, SLOT(testVideoCapture()));
	connect(m_pushButtonStart_Stop, SIGNAL(clicked()), this, SLOT(onPushButtonStart_Stop()));
	connect(m_pushButtonOpenVideo, SIGNAL(clicked()), this, SLOT(onPushbuttonOpenvideo()));
	connect(m_pushButtonSaveCurrentFrame, SIGNAL(clicked()), this, SLOT(onPushButtonSaveImage()));
	connect(m_videoSpeedSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onChangeVideoSpeed(int)));
	connect(m_interThreshSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onChangeIntersectionAreaPercent(int)));
	connect(m_minimumSizeForStrawRectSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onChangeMinimumStrawSize(int)));
	connect(m_pushButtonAddClassAndSub, SIGNAL(clicked()), this, SLOT(onPushButtonAddClassSubClass()));
	connect(m_pushButtonRemoveClassAndSub, SIGNAL(clicked()), this, SLOT(onPushButtonRemoveClassSubClass()));

	connect(m_dialogInfo,SIGNAL(onVectorElements(classAndSubClassesType)),this, SLOT(onProcessNewAddedClassData(classAndSubClassesType)));

	connect(m_dialogRemClassInfo, SIGNAL(onRemoveClassName(const QString&)), this, SLOT(onProcessRemoveCurrentClass(const QString&)));


}

void VideoShowDemo::drawCurrentDetectedRectsByAlgorithm()
{
	/*for (QList<QGraphicsRectItem*>::const_iterator it(m_detectedRectsByAlgGraphicsVec.begin()); it != m_detectedRectsByAlgGraphicsVec.end(); ++it) {
	//	std::cout << "current list rect " << (*it)->boundingRect().x() << "  " << (*it)->boundingRect().y() << "  " << (*it)->boundingRect().width() << "  " << (*it)->boundingRect().height();
	//m_graphicsViewVideo->scene()->addRect((*it)->rect(), QPen(QBrush(QColor(0, 255, 0)), 1.0 / 1.0));

	m_graphicsViewVideo->scene()->addRect(QRect(10,10,100,100), QPen(QBrush(QColor(0, 255, 0)),
	1.0 / 1.0));
	std::cout << "ADDED RECT ARTASH   " << (*it)->rect().x() << "  " << (*it)->rect().y() << "  " << (*it)->rect().width() << "  " << (*it)->rect().height() << std::endl;

	}*/
	/*for (int i = 0; i < m_detectedRectsByAlg.size(); ++i) {
		//	std::cout << "Comming rect " << m_detectedRectsByAlg[i].x << "   " << m_detectedRectsByAlg[i].y << "   " << m_detectedRectsByAlg[i].width << "   " << m_detectedRectsByAlg[i].height << std::endl;
		m_graphicsViewVideo->scene()->addRect(QRect(m_detectedRectsByAlg[i].x, m_detectedRectsByAlg[i].y, m_detectedRectsByAlg[i].width, m_detectedRectsByAlg[i].height), QPen(QBrush(QColor(0, 255, 0)),
			1.0 / 1.0));
	}*/
	//cv::imshow("curr frame", m_currentFrameOrig);
	//cv::waitKey(0);
}


void VideoShowDemo::testVideoCapture()
{

	//std::cout << "INtersection area video !!!!!!!!!!!!! " << m_intersectionAreaPercent << std::endl;
	//m_timer->stop();
	//m_rectsItem.clear();
	m_cropedRectFromCurrentFrame.clear();
	m_detectedRectsByAlgGraphicsVec.clear();
	m_detectedRectsByAlg.clear();
	m_selectedRectsCountOnCurrentFrame = 0;
	m_rectsItem.clear();
	//	m_selectedRectByUser.clear();
	if (!m_videoCapture.isOpened()) {
		std::cout << "Video capture is not valid \n";
		exit(1);
	}
	cv::Mat frame;// = cv::imread("D:\\Scopic Projects\\x64\\Release\\imageData\\278_orig.png");
	m_videoCapture >> frame >> frame;
	//std::cout << "Frame size " << frame.cols << "   " << frame.rows << std::endl;
	if (frame.empty()) {
		//m_timer->stop();

		return;
	}
	//cv::resize(frame, frame, cv::Size(),	1.5, 1.5);
	//frame = ~frame;
	static int i = 0;
	m_currentFrameOrig = frame.clone();


	//	m_textRegionExtractor->setFrame(frame);

	m_currentFrame = frame.clone();

	//m_redStrawDetector.setCurrentImg(frame);

	//filterDetectedRectsByAlgorithmInRange(m_detectedRectsByAlg);
	//std::cout << "After filtering detected rectangles size " << m_detectedRectsByAlg.size() << std::endl;


	cv::cvtColor(frame, frame, CV_BGR2RGB);
	QPixmap p = QPixmap::fromImage(QImage((uchar*)frame.data, frame.cols, frame.rows, frame.step1(), QImage::Format_RGB888));

	m_scenvideo->clear();
	m_scenvideo->setSceneRect(p.rect());
	m_scenvideo->addPixmap(p);
	//std::cout << "COunt of rects !!!  " << m_rectsItem.size() << std::endl;

	/*if (m_isStartedDetectionAlgorithm) {
		++m_indexOfFrameStartedAlg;
		for (int i = 0; i < m_detectedRectsByAlg.size(); ++i) {
			QGraphicsRectItem* currentRectFromAlg = new QGraphicsRectItem;
			currentRectFromAlg->setRect((qreal)m_detectedRectsByAlg[i].x, (qreal)m_detectedRectsByAlg[i].y, (qreal)m_detectedRectsByAlg[i].width, (qreal)m_detectedRectsByAlg[i].height);
			currentRectFromAlg->setPen(QPen(QColor(0, 255, 0)));
			
			m_detectedRectsByAlgGraphicsVec.push_back(currentRectFromAlg);
			m_graphicsViewVideo->scene()->addItem(currentRectFromAlg);

		}
		m_countOfAllRedRasp = m_detectedRectsByAlgGraphicsVec.size() + m_rectsItem.size();

		m_redStrawDetector.m_strawTracker.setCurrentImage(m_currentFrame);
		
		if (m_selectedRectByUser.size() != 0) {
			std::cout << "User selected rects size!!!!!!!!!!!!!!!! " << m_selectedRectByUser.size() << std::endl;
			std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
			for (int k = 0; k < m_selectedRectByUser.size(); ++k) {
				if (!isCurrentRectExistsInTrackedRects(cv::Rect2d(m_selectedRectByUser[k]), rectsVecFromTracker, m_intersectionAreaPercent) && !m_isCurrentUserSelRectAddedToTracker[k]) {
					m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_selectedRectByUser[k].x, m_selectedRectByUser[k].y, m_selectedRectByUser[k].width, m_selectedRectByUser[k].height));
					m_isCurrentUserSelRectAddedToTracker[k] = true;
				}
			}

		}
	
		if (m_indexOfFrameStartedAlg == 1) {
			
			for (int k = 0; k < m_detectedRectsByAlg.size(); ++k) {
				m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_detectedRectsByAlg[k].x, m_detectedRectsByAlg[k].y, m_detectedRectsByAlg[k].width, m_detectedRectsByAlg[k].height));
			}

		


			std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
		
		}

		else {
			m_redStrawDetector.m_strawTracker.updateTrackerInfo(m_currentFrame);
			std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
			std::vector<cv::Rect2d> rectsVec = m_redStrawDetector.m_strawTracker.getRectsFromTrackVector();
			if (rectsVec.size() == rectsVecFromTracker.size()) {
				std::cout << "size of vects are equal " << std::endl;
			}
			std::cout << "User selected rects size!!!!!!!!!!!!!!!! " << m_selectedRectByUser.size() << std::endl;;
			std::vector<cv::Rect2d> rectsForRemove;
			for (int i = 0; i < rectsVecFromTracker.size(); ++i) {
				if (!isRectInRange(rectsVecFromTracker[i])) {
					rectsForRemove.push_back(m_redStrawDetector.m_strawTracker.getTrackerCurrentRectByIndex(i));
				}
			}
			if (rectsForRemove.size() != 0) {
				//		std::cout << "Artash  !!!!!!!!!!!!!!!!! go to !!!!!!!!!!!!!!!" << std::endl;
				std::cout << "before remove rects size " << m_redStrawDetector.m_strawTracker.getTrackedStrawberries().size() << std::endl;
				m_redStrawDetector.m_strawTracker.removeStrawFromTrack(rectsForRemove);
				rectsVecFromTracker.clear();
				rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
				std::cout << "after remove rects size " << m_redStrawDetector.m_strawTracker.getTrackedStrawberries().size() << std::endl;
			}
			//m_redStrawDetector.m_strawTracker.removeStrawFromTrack(m_detectedRectsByAlg);
			for (int g = 0; g < m_detectedRectsByAlg.size(); ++g) {
				if (!isCurrentRectExistsInTrackedRects(cv::Rect2d(m_detectedRectsByAlg[g]), rectsVecFromTracker, m_intersectionAreaPercent)) {
					std::cout << "Info rect and trakcer rects " << std::endl;
					std::cout << "Tracker rects  " << std::endl;
					for (int h = 0; h < rectsVecFromTracker.size(); ++h) {
						std::cout << rectsVecFromTracker[h].x << "   " << rectsVecFromTracker[h].y << "  " << rectsVecFromTracker[h].width << "   " << rectsVecFromTracker[h].height << std::endl;
					}
					//std::cout << "Detected rect into " << std::endl;
					std::cout << m_detectedRectsByAlg[g].x << "   " << m_detectedRectsByAlg[g].y << "   " << m_detectedRectsByAlg[g].width << "   " << m_detectedRectsByAlg[g].height << std::endl;
					if (m_detectedRectsByAlg[g].width > m_minimumStrawSize && m_detectedRectsByAlg[g].height > m_minimumStrawSize) {
						std::cout << "CURRENT RECT IS GREATER THAN SIZE !!!!!!" << std::endl;
						m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_detectedRectsByAlg[g].x, m_detectedRectsByAlg[g].y, m_detectedRectsByAlg[g].width, m_detectedRectsByAlg[g].height));
					}
					//	std::cout << "Adding new rect to existing tracker rect!!!!!!!!!!!!!!!! " << std::endl;
				}
			}
			for (int i = 0; i < rectsVecFromTracker.size(); ++i) {
				QGraphicsRectItem* currentRectFromAlg = new QGraphicsRectItem;
				//	std::cout << "After update fnction " << std::endl;
				currentRectFromAlg->setRect((qreal)rectsVecFromTracker[i].x, (qreal)rectsVecFromTracker[i].y, (qreal)rectsVecFromTracker[i].width, (qreal)rectsVecFromTracker[i].height);
				if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectValidityByIndex(i)) {
					currentRectFromAlg->setPen(QPen(QColor(255, 0, 0)));
				}
				else {
					currentRectFromAlg->setBrush((QColor(0, 0, 255)));
				}
				m_detectedRectsByAlgGraphicsVec.push_back(currentRectFromAlg);
				m_graphicsViewVideo->scene()->addItem(currentRectFromAlg);


			}
			//	m_timer->stop();
		}

		for (int g = 0; g < m_redStrawDetector.m_strawTracker.getRectsFromTrackVector().size(); ++g) {
			if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectStateForCounted(g) == false && m_redStrawDetector.m_strawTracker.getTrackerCurrentRectValidityByIndex(g) == true) {
				m_redStrawDetector.m_strawTracker.changeCurrentRectStateForCounted(g, true);
				++m_totalCountOfRedStrawberries;
			}
		}

		std::ostringstream textForCountOfRasbCh;
		textForCountOfRasbCh << m_totalCountOfRedStrawberries;
		std::string textForCountOfRasb = "Count Of Straw " + textForCountOfRasbCh.str();
		m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
	}*/

	//++m_currentFrameIndex;



}
/*
void VideoShowDemo::extractRectClusters(const std::vector<cv::Rect>& inputRects, std::vector<lineOfRects>& clusterOfLines)
{
clusterOfLines.clear();
cv::Rect tmpRect;
for (int i = 0; i < inputRects.size(); ++i) {
tmpRect = inputRects.at(i);
if (clusterOfLines.size() == 0) {
lineOfRects tmpLine;
tmpLine.push_back(tmpRect);
clusterOfLines.push_back(tmpLine);
}
else {
for (int j = 0; j < clusterOfLines.size(); ++j) {
for (int h = 0; h < clusterOfLines.at(j).size(); ++h) {

if (((tmpRect.y > clusterOfLines.at(j).at(h).y && (tmpRect.y - clusterOfLines.at(j).at(h).y) > clusterOfLines.at(j).at(h).height)) ||
((tmpRect.y < clusterOfLines.at(j).at(h).y && (clusterOfLines.at(j).at(h).y - tmpRect.y) > tmpRect.height))  ) {
if (h == clusterOfLines.at(j).size() - 1){
lineOfRects tmpLine;
tmpLine.push_back(clusterOfLines.at(j).at(h));
clusterOfLines.push_back(tmpLine);
break;
}
else {
continue;
}

}
else {
clusterOfLines.at(j).push_back(tmpRect);
break;
}

}
}
}
}

}*/

void VideoShowDemo::onPushButtonStart_Stop()
{
	m_isStartedDetectionAlgorithm = true;

	if (m_pushButtonStart_Stop->text() == "START") {
		std::cout << "Selected Rects Count!!!!!  " << m_rectsItem.size();
		for (int i = 0; i < m_rectsItem.size(); ++i) {
			cv::Rect tmpRect;
			tmpRect.x = (int)m_rectsItem[i]->rect().x();
			tmpRect.y = (int)m_rectsItem[i]->rect().y();
			tmpRect.width = (int)m_rectsItem[i]->rect().width();
			tmpRect.height = (int)m_rectsItem[i]->rect().height();
			cv::Mat tmpMat = m_currentFrame(tmpRect);
			std::string tmpSavingImagePath = "cropedImageData/" + m_currentSelectedClassName.first + "/" 
				+ m_currentSelectedClassName.second + "/"  + m_currentSelectedClassName.first  + "_" + m_currentSelectedClassName.second + "_" + std::to_string((long)m_indexOfCurrentSavedImage) + ".png";
			cv::imwrite(tmpSavingImagePath, tmpMat);
			++m_indexOfCurrentSavedImage;
			
			std::string tmpInfoFilePath = "cropedImageData/" + m_currentSelectedClassName.first + "/"
				+ m_currentSelectedClassName.second + "/" + "infoFile.txt";
			std::fstream infoFile;
			infoFile.open(tmpInfoFilePath);
			infoFile << m_indexOfCurrentSavedImage << "\n";
			infoFile.close();
			

		}
		m_timer->start(m_videoSpeedSpinBox->value());
		m_pushButtonStart_Stop->setStyleSheet("QPushButton {background-color: #48D1CC; color: red;}");
		m_pushButtonStart_Stop->setText("STOP");

		return;
	}
	if (m_pushButtonStart_Stop->text() == "STOP") {
		m_timer->stop();
		m_pushButtonStart_Stop->setStyleSheet("QPushButton {background-color: #48D1CC; color: blue;}");
		m_pushButtonStart_Stop->setText("START");
	}
	
}

void VideoShowDemo::onRightClickMenu()
{
	std::cout << "Right Click Menu item " << std::endl;

}
void VideoShowDemo::onRecentOpenFilesStrawBerry(QAction* action)
{
	m_indexOfCurrentSavedImage = 0;
	std::cout << "CAllled StrawBerry cllass !!!!!!!!!!" << std::endl;
	QWidget *widget1 = action->parentWidget();
	QMenu* menu1;
	if (widget1) {
		menu1 = dynamic_cast<QMenu*>(widget1);
		QString className = "cropedImageData/" + menu1->title();
		if (!QDir(className).exists()) {
			QDir().mkdir(className);
		}
	}
	QString subClassName = QString("cropedImageData/") + menu1->title() + '/' + action->text();
	//std::cout << "Subfolder text " << subfolderPath.toStdString() << std::endl;
	
	if (!QDir(subClassName).exists()) {				
		QDir().mkdir(subClassName);
	//	m_indexOfCurrentSavedImage = 0;
		//infoFile << m_indexOfCurrentSavedImage << "\n";
		//infoFile.close();
	}
	//else {
	std::string infoFilePath = subClassName.toStdString() + "/" + "infoFile.txt";
	
		//std::string infoFilePath = subClassName.toStdString() + "/" + "infoFile.txt";
		std::ifstream infoFileR;		
		infoFileR.open(infoFilePath, std::ios::app);
		std::string tmpLine;
		std::getline(infoFileR, tmpLine);
		infoFileR.close();
		std::cout << "TMp line value!!!!  " << tmpLine << std::endl;
		if (tmpLine.size() >= 1 /*&& m_indexOfCurrentSavedImage != 0*/) {
			m_indexOfCurrentSavedImage = m_indexOfCurrentSavedImage + std::atoi(tmpLine.c_str());
			std::ofstream infoFile;
			infoFile.open(infoFilePath);
			infoFile << m_indexOfCurrentSavedImage << "\n";
			infoFile.close();
		}
		
	
	//}

	
	m_currentSelectedClassName.first = (menu1->title()).toStdString();
	m_currentSelectedClassName.second = (action->text()).toStdString();
	std::string selectedClassFullName = "Select " + menu1->title().toStdString() + " " + action->text().toStdString();
	m_selectedClassNameFor->setText(selectedClassFullName.c_str());
	/*if (action->text() == "Red") {
		
		m_selectedClassNameFor->setText("Select Red Strawberry");
	}
	else if (action->text() == "Green") {
		
		m_selectedClassNameFor->setText("Select Green Strawberry");
	} else if (action->text() == "Yellow"){
	
		m_selectedClassNameFor->setText("Select Yellow Strawberry");
	} else if (action->text() == "WeedType") {
		m_selectedClassNameFor->setText("Select Weed");
	} else if (action->text() == "FlowerType") {
		m_selectedClassNameFor->setText("Select Flower");
	} else if (action->text() == "RunnerType") {
		m_selectedClassNameFor->setText("Select Runner");
	}*/
	
}

void VideoShowDemo::onPushButtonAddClassSubClass()
{
	m_dialogInfo->clearVectorInfo();
	m_dialogInfo->clearContent();
	m_dialogInfo->show();
	m_dialogInfo->setStartValue(false);
}

void VideoShowDemo::onPushButtonRemoveClassSubClass()
{
	m_dialogRemClassInfo->show();
	m_dialogRemClassInfo->addItemsToMenu(m_listOfExistingMenuItems);

	//m_dialogInfo->show();
}

void VideoShowDemo::onProcessNewAddedClassData(classAndSubClassesType tmpClassAndSubClass)
{

	//std::cout << "Adding new classes!!!!!!!!!!!  " << std::endl;
	if (tmpClassAndSubClass.second.size() != 0) {

		QMenu* submenuA = m_xmenu->addMenu(tmpClassAndSubClass.first.c_str());
		for (int i = 0; i < tmpClassAndSubClass.second.size(); ++i) {
			QAction* actionA_Setup1 = submenuA->addAction(tmpClassAndSubClass.second[i].c_str());
		}
		connect(submenuA, SIGNAL(triggered(QAction*))
			, SLOT(onRecentOpenFilesNewClass(QAction*)));
		m_listOfExistingMenuItems.push_back(submenuA);
	}
	
	m_dialogInfo->close();
	
	

}

void VideoShowDemo::onProcessRemoveCurrentClass(const QString& remClassName)
{

//	m_xmenu->clear();
	//m_dialogRemClassInfo->close();
	m_xmenu->clear();
	std::vector<QMenu*> vectorOfAllMenuItems;
	for (int i = 0; i < m_listOfExistingMenuItems.size(); ++i) {
		if (remClassName.toStdString() == m_listOfExistingMenuItems[i]->title().toStdString()) {
			continue;
		}
		vectorOfAllMenuItems.push_back(m_listOfExistingMenuItems[i]);
	}
	m_listOfExistingMenuItems.clear();
	m_listOfExistingMenuItems = vectorOfAllMenuItems;
	for (int i = 0; i < m_listOfExistingMenuItems.size(); ++i) {
		QMenu* submenu = m_xmenu->addMenu(m_listOfExistingMenuItems[i]->title());

		QAction* tmpAction;
		for (int j = 0; j < m_listOfExistingMenuItems[i]->actions().size(); ++j) {
			tmpAction = submenu->addAction(m_listOfExistingMenuItems[i]->actions().at(j)->text());
		}
	}

}


void VideoShowDemo::onRecentOpenFilesNewClass(QAction* action)
{
	std::cout << "Press and processing action !!!!!!!!!" << std::endl;
	m_indexOfCurrentSavedImage = 0;
	QWidget *widget1 = action->parentWidget();
	QMenu* menu1;
	if (widget1) {
		menu1 = dynamic_cast<QMenu*>(widget1);
		QString className = "cropedImageData/" + menu1->title();
		if (!QDir(className).exists()) {
			QDir().mkdir(className);
		}
	}
	QString subClassName = QString("cropedImageData/") + menu1->title() + '/' + action->text();
	//std::cout << "Subfolder text " << subfolderPath.toStdString() << std::endl;

	if (!QDir(subClassName).exists()) {

		QDir().mkdir(subClassName);
		m_indexOfCurrentSavedImage = 0;
		//infoFile << m_indexOfCurrentSavedImage << "\n";
		//infoFile.close();
	}
	//else {
	std::string infoFilePath = subClassName.toStdString() + "/" + "infoFile.txt";

	//std::string infoFilePath = subClassName.toStdString() + "/" + "infoFile.txt";
	std::ifstream infoFileR;
	infoFileR.open(infoFilePath, std::ios::app);
	std::string tmpLine;
	std::getline(infoFileR, tmpLine);
	infoFileR.close();
	std::cout << "TMp line size !!!!  " << tmpLine.size() << std::endl;
	if (tmpLine.size() >= 1) {
		m_indexOfCurrentSavedImage = m_indexOfCurrentSavedImage + std::atoi(tmpLine.c_str());
	}

	std::ofstream infoFile;
	infoFile.open(infoFilePath);
	infoFile << m_indexOfCurrentSavedImage << "\n";
	infoFile.close();
	//}


	m_currentSelectedClassName.first = (menu1->title()).toStdString();
	m_currentSelectedClassName.second = (action->text()).toStdString();

	//if (action->text() == "FlowerType") {
	std::string textInLine = "Select " + action->text().toStdString();
	m_selectedClassNameFor->setText(textInLine.c_str());
//	}





}


void VideoShowDemo::onPushbuttonOpenvideo()
{
	filename = QFileDialog::getOpenFileName(
		this,
		tr("Open video"),
		".",
		"Video files (*.avi *.mp4 *.png *.avi)");

	if (filename.isEmpty()) {
		m_timer->stop();
		return;
	}

	m_videoCapture.open(filename.toStdString());
	m_currentFrameIndex = 0;
	
	if (!m_videoCapture.isOpened()) {
		qDebug() << "VideoCapture is not openned fileame is not valid" << filename;
		m_timer->stop();
		return;
	}
	m_timer->start(m_videoSpeedSpinBox->value());
}

void VideoShowDemo::onPushButtonSaveImage()
{
	//std::cout << "saving image " << std::endl;

	std::ostringstream currentFrameIndexCh;
	currentFrameIndexCh << m_currentFrameIndex;
	std::string savedImagePath = "imageData\\" + currentFrameIndexCh.str()/*std::to_string(long(m_currentFrameIndex))*/ + ".png";
	std::string savedOrigImagePath = "imageData\\" + currentFrameIndexCh.str()/*std::to_string(long(m_currentFrameIndex))*/ + "_orig.png";
	QList<QGraphicsRectItem*>::iterator it;

	cv::Mat saveImg = m_currentFrame.clone();
	if (saveImg.cols != 0 && saveImg.rows != 0) {

		cv::imwrite(savedOrigImagePath, m_currentFrameOrig);
	}
	for (it = m_rectsItem.begin(); it != m_rectsItem.end(); ++it) {
	
		//std::cout << "Current list item init" << std::endl;
		cv::Rect drawRect;
		
		drawRect.x = (*it)->rect().x();
		drawRect.y = (*it)->rect().y();
		drawRect.width = (*it)->rect().width();
		drawRect.height = (*it)->rect().height();
		//std::cout << "Current list item end" << std::endl;
		cv::rectangle(saveImg, drawRect, cv::Scalar(0, 0, 255), 2);
	}




//	std::cout << "saving image  with path " << savedImagePath << std::endl;
	if (saveImg.cols != 0 && saveImg.rows != 0) {

		cv::imwrite(savedImagePath, saveImg);
	}
}

void VideoShowDemo::onChangeVideoSpeed(int newValue)
{
	int getSpinBoxCurrentValue = m_videoSpeedSpinBox->value();
	m_timer->setInterval(getSpinBoxCurrentValue);
}

void VideoShowDemo::onChangeIntersectionAreaPercent(int intersecPer)
{
	m_intersectionAreaPercent = m_interThreshSpinBox->value();
//	std::cout << "Changing intersection  part   "<<m_intersectionAreaPercent<<std::endl;
	m_interThreshSpinBox->setValue(intersecPer);

}

void VideoShowDemo::onChangeMinimumStrawSize(int minimumStrawSize)
{
	m_minimumStrawSize = minimumStrawSize;
}

QPixmap VideoShowDemo::getStringImg(const QString& str, const QRect& rect)
{
	QString qStr = str;
	QStringList qStrList = qStr.split("\n");
	int maxLength = qStrList.at(0).length();
	for (int i = 0; i < qStrList.size(); ++i) {
		if (qStrList.at(i).length() > maxLength) {
			maxLength = qStrList.at(i).length();
		}
	}

	QFont font("times", rect.width() / maxLength);
	QPixmap pixmap(rect.width() + 4, rect.height() + 4);
	pixmap.fill(QColor(255, 255, 255, 255));
	QPainter painter(&pixmap);
	QPen pen(QColor(255, 0, 0, 255));
	painter.setPen(pen);
	painter.setFont(font);
	painter.drawText(pixmap.rect(), Qt::AlignHCenter, qStr, &pixmap.rect());

	return pixmap;
}

bool VideoShowDemo::isRectInRange(const cv::Rect& currentRect)
{
	if (currentRect.width == 0 || currentRect.height == 0) {
		return false;
	}
	if (currentRect.x > 20 && currentRect.y > 20 && ((currentRect.x + currentRect.width) < (m_currentFrame.cols - 20)) && ((currentRect.y + currentRect.height) < (m_currentFrame.rows - 20))) {
		return true;
	}
	else {
		return false;
	}

}

bool VideoShowDemo::isRectsAreIntersecting(const cv::Rect2d& rect1, const cv::Rect2d& rect2)
{
	if ((rect1.x + rect1.width < rect2.x) || (rect2.x + rect2.width < rect1.x)
		|| (rect1.y + rect1.height < rect2.y) || (rect2.y + rect2.height < rect1.y)) {
		return false;
	}
	return true;
	
}


bool VideoShowDemo::isRectsContainingEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2)
{
	if (!isRectsAreIntersecting(rect1, rect2)) {
		return false;
	}
	else if ((rect1.x > rect2.x && rect1.y > rect2.y && (rect1.width + rect1.x < rect2.width + rect2.x) && (rect1.height + rect1.y < rect2.height + rect2.y))
		||
		(rect1.x < rect2.x && rect1.y < rect2.y && (rect1.width + rect1.x> rect2.width + rect2.x) && (rect1.height + rect1.y > rect2.height + rect2.y))) {

		return true;

	}
	return false;
}

bool VideoShowDemo::isRectsMuchCloseToEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2, const unsigned int& thresholdValue)
{
	double area12 = (float)rect1.area() / (float)rect2.area() * 100;
	double area21 = (float)rect2.area() / (float)rect1.area() * 100;
	if (isRectsContainingEachOther(rect1, rect2)) {
		return true;
	}
	if (isRectsAreIntersecting(rect1, rect2)) {
		//std::cout << "RECTS ARE INTRSECTING " << std::endl;
		if (rect1.area() <= rect2.area() &&
			(((float)rect1.area() / (float)rect2.area() * 100) > thresholdValue) /*|| (((float)rect1.area() / (float)rect2.area() * 100) > thresholdValue)*/) {
			//  std::cout << "AREAAAAAAAAA12 " << area12 << std::endl;
			return true;
		}
		else if (rect1.area() > rect2.area() && (((float)rect2.area() / (float)rect1.area() * 100) > thresholdValue)) {
			//std::cout << "AREAAAAAAAAA22 " << area21 << std::endl;
			return true;
		}
		else {
			return false;
		}
	}
	else {
		//	std::cout << "Reason for adding rects are not intersection " << std::endl;
		return false;
	}
}

bool   VideoShowDemo::isCurrentRectExistsInTrackedRects(const cv::Rect2d& currentRect, const std::vector<cv::Rect2d>& trackerRectsVec, const unsigned int& thresholdValue)
{
	int countOfCompared = 0;
	for (int i = 0; i < trackerRectsVec.size(); ++i) {
		if (isRectsMuchCloseToEachOther(currentRect, trackerRectsVec[i], thresholdValue)) {

			return true;
		}

	}
	return false;
}


void VideoShowDemo::filterDetectedRectsByAlgorithmInRange(std::vector<cv::Rect>& inputRectsVec)
{
	std::vector<cv::Rect>::iterator iterVec;
	for (iterVec = inputRectsVec.begin(); iterVec != inputRectsVec.end();) {
		if (isRectInRange(*iterVec)) {
			++iterVec;
		}
		else {
			iterVec = m_detectedRectsByAlg.erase(iterVec);
		}
	}
}

void VideoShowDemo::addNewClassAndSublassIntoMenu(classAndSubClassesType tmpClassAndSubClass)
{
	
	if (tmpClassAndSubClass.second.size() != 0) {

		QMenu* submenuA = m_xmenu->addMenu(tmpClassAndSubClass.first.c_str());
		for (int i = 0; i < tmpClassAndSubClass.second.size(); ++i) {
			QAction* actionA_Setup1 = submenuA->addAction(tmpClassAndSubClass.second[i].c_str());
		}
	}

}


bool VideoShowDemo::splitStringIntoParts(const std::string &inputStr, std::vector<std::string> &outputVec, char symbolToSplit)
{
	unsigned int pos = inputStr.find(symbolToSplit);
	unsigned int initialPos = 0;
	outputVec.clear();

	// Decompose statement
	while (pos != std::string::npos) {
		outputVec.push_back(inputStr.substr(initialPos, pos - initialPos + 1));
		initialPos = pos + 1;

		pos = inputStr.find(symbolToSplit, initialPos);
	}

	// Add the last one
	int minFromSym;
	if (pos < inputStr.size()) {
		minFromSym = pos;
	}
	else {
		minFromSym = inputStr.size();
	}

	outputVec.push_back(inputStr.substr(initialPos, minFromSym - initialPos + 1));

	return true;
}

bool VideoShowDemo::eventFilter(QObject* object, QEvent* event)
{

	if (m_pushButtonStart_Stop->text() == "STOP" || !m_isStartedDetectionAlgorithm) {

		return false;
	}

	static QPointF rectStartPoint;

	QGraphicsSceneMouseEvent* ev = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
	if (object == m_scenvideo && ev != 0) {

	
		QGraphicsItem* item = m_scenvideo->itemAt(ev->scenePos(), QTransform());
		qDebug() << ev << item;
		QGraphicsPixmapItem* imgItem = qgraphicsitem_cast<QGraphicsPixmapItem*>(item);
		if (imgItem == 0) {
			QGraphicsRectItem* rectItem = qgraphicsitem_cast<QGraphicsRectItem*>(item);
			if (rectItem == 0) {
			
				m_drawRect = false;
				return QWidget::eventFilter(object, event);
			}
		}

		if (ev->type() == QEvent::GraphicsSceneMousePress && ev->button() == Qt::LeftButton) {
			unsigned sizeForCropRectWidth = m_cropRectWidthSpinBox->value();
			unsigned sizeForCropRectHeight = m_cropRectHeightSpinBox->value();
			std::cout << "Size for croped rect " <<sizeForCropRectWidth<< std::endl;
			rectStartPoint = ev->scenePos();
			if (rectStartPoint.x() > sizeForCropRectWidth/2 && rectStartPoint.y() > sizeForCropRectHeight/2) {
				QGraphicsRectItem* rectItem = m_graphicsViewVideo->scene()->addRect(
						QRectF(QPointF(ev->scenePos().x()- sizeForCropRectWidth/2,ev->scenePos().y()- sizeForCropRectHeight/2),QSizeF(sizeForCropRectWidth,sizeForCropRectHeight)), QPen(QBrush(QColor(128, 0, 255)),
							1.0 / 1.0));
				m_rectsItem.push_back(rectItem);
				m_drawRect = true;
			}
		}
		
	
		else if (ev->type() == QEvent::GraphicsSceneMouseRelease && ev->button() == Qt::RightButton) {
			if (!m_rectsItem.isEmpty() /*&& m_selectedRectsCountOnCurrentFrame > 0*/) {
				--m_selectedRectsCountOnCurrentFrame;
				m_graphicsViewVideo->scene()->removeItem(m_rectsItem.back());
				m_rectsItem.pop_back();
		
			}
			m_drawRect = false;
		}
		else if (ev->type() == QEvent::GraphicsSceneMouseDoubleClick && ev->button() == Qt::LeftButton) {
			//std::cout << "double click the last " << std::endl;
			unsigned int remElIndex;
			QPointF tmpClickedPoint;
			tmpClickedPoint.setX(ev->scenePos().x());
			tmpClickedPoint.setY(ev->scenePos().y());
		

		}
	}
	return QWidget::eventFilter(object, event);
}

VideoShowDemo::~VideoShowDemo()
{
	std::cout << "Desctructor call " << std::endl;
	std::ofstream myfile;
	myfile.open("infoDataClasses.txt");
	std::cout << "Size of vector " << m_listOfExistingMenuItems.size() << std::endl;
	for (int i = 0; i < m_listOfExistingMenuItems.size(); ++i) {
		std::cout << "Objct name " << m_listOfExistingMenuItems[i]->objectName().toStdString() << std::endl;
		myfile << m_listOfExistingMenuItems[i]->title().toStdString() << " ";
		for (int j = 0; j <  m_listOfExistingMenuItems[i]->actions().size(); ++j) {
			myfile << m_listOfExistingMenuItems[i]->actions().at(j)->text().toStdString() << " ";
		}
		myfile << "\n";
	}
//	myfile << "Writing this to a file.\n";
	myfile.close();
}