#include <iostream>

#include "DialogInfoRemove.h"


DialogInfoRemove::DialogInfoRemove(QWidget *parent)
	: QDialog(parent)
{
	createMembers();

}
void DialogInfoRemove::createMembers()
{
	m_classMenuBar = new QMenuBar(this);
	m_classMenu = new QMenu("Remove Class");
	m_classMenuBar->addMenu(m_classMenu);



}

void DialogInfoRemove::onSelectRemovedClass(QAction* remClass)
{
	//if (remClass->parentWidget() != NULL) {
	QWidget *widget1 = remClass->parentWidget();
	if (widget1) {
		QMenu* menu1 = dynamic_cast<QMenu*>(widget1);
		
	
			std::vector<QMenu*> newDataToAddMenu;
			//std::vector<QString> newDataToAddMenu;
			int indexOfEl = 0;
			for (int i = 0; i < m_currentItemsInMenu.size(); ++i) {
				if (m_currentItemsInMenu[i]->title().toStdString() != menu1->title().toStdString()) {
					newDataToAddMenu.push_back(m_currentItemsInMenu[i]);
				}
			}

			m_classMenu->clear();
			addItemsToMenu(newDataToAddMenu);
			emit onRemoveClassName(menu1->title());
		

	}
}

void DialogInfoRemove::addItemsToMenu(const std::vector<QMenu*>& dataToAdd)
{
	m_classMenu->clear();
	setCurrentItemsInMenu(dataToAdd);
	for (int i = 0; i < dataToAdd.size(); ++i) {

		QMenu* tmpMenu =  m_classMenu->addMenu(dataToAdd[i]->title());
		QAction* tmpAction =  tmpMenu->addAction("Remove class");
		//connect(tmpAction, SIGNAL(trige()), SLOT(onSelectRemovedClass()));
		connect(tmpMenu, SIGNAL(triggered(QAction*)), SLOT(onSelectRemovedClass(QAction*)));
		
	}
}



void DialogInfoRemove::setCurrentItemsInMenu(const std::vector<QMenu*>& currentItemsInMenu)
{
	m_currentItemsInMenu.clear();
	for (int i = 0; i < currentItemsInMenu.size(); ++i) {
		m_currentItemsInMenu.push_back(currentItemsInMenu[i]);
	}
}


std::vector<QMenu*> DialogInfoRemove::getCurrentItemsInMenu()
{
	return m_currentItemsInMenu;
}

