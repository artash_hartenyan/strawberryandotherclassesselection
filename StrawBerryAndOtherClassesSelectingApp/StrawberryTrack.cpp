#include "StrawberryTrack.h"

StrawberryTracker::StrawberryTracker(const std::string& trackName, const cv::Mat& initImg) : m_trackerName(trackName),
																							 m_currentImg(initImg)
{
	m_strawMultiTracker = new cv::MultiTracker(trackName);
	
	//m_strawMultiTracker->add(m_currentImg, cv::Rect2d());
	std::cout << "Tracker objects count init " << m_strawMultiTracker->objects.size() << std::endl;
}

void StrawberryTracker::addNewStrawToTrack(const cv::Rect2d& newRect)
{

	m_strawMultiTracker->add(m_currentImg, newRect);
	m_trackerObjRects.push_back(newRect);
	m_isTrackedRectValid.push_back(true);
	m_isTrackedRectAddedToCount.push_back(false);
	//for (int h = 0; h < m_strawMultiTracker->objects.size(); ++h) {
	//	std::cout << "our rects vec" << m_strawMultiTracker->objects[h].x << "  " << m_strawMultiTracker->objects[h].y << "   " << m_strawMultiTracker->objects[h].width << "  " << m_strawMultiTracker->objects[h].height << std::endl;
	//	std::cout << "tracker rects vec" << m_trackerObjRects[h].x << "  " << m_trackerObjRects[h].y << "   " << m_trackerObjRects[h].width << "  " << m_trackerObjRects[h].height << std::endl;
	//}
}


void StrawberryTracker::changeCurrentRectValidity(const unsigned int& indexOfRect, const bool& validity)
{
	m_isTrackedRectValid[indexOfRect] = validity;
}


void StrawberryTracker::changeCurrentRectStateForCounted(const unsigned int& indexOfRect, const bool& stateForCounted)
{
	m_isTrackedRectAddedToCount[indexOfRect] = stateForCounted;
}

void StrawberryTracker::removeStrawFromTrack(const std::vector<cv::Rect2d>& remRectVec)
{
	
	//m_strawMultiTracker = NULL;
	//delete m_strawMultiTracker;
	//for (int i = 0; i < m_strawMultiTracker->objects.size(); ++i) {
	//	std::cout << "tracker rect origin " << m_strawMultiTracker->objects[i].x << "  " << m_strawMultiTracker->objects[i].y << "  " << m_strawMultiTracker->objects[i].width << "  " << m_strawMultiTracker->objects[i].height << std::endl;
//		std::cout << "tracker rect our" << m_trackerObjRects[i].x << "  " << m_trackerObjRects[i].y << "  " << m_trackerObjRects[i].width << "  " << m_trackerObjRects[i].height << std::endl;
//	}
	*m_strawMultiTracker = cv::MultiTracker(m_trackerName);
	//m_strawMultiTracker->~MultiTracker();

	std::cout << "Artash remove rects trakcer size  "<<m_strawMultiTracker->objects.size() << std::endl;
	//m_strawMultiTracker = new  cv::MultiTracker(m_trackerName);
	//return;
	//m_strawMultiTracker->
	std::cout << "after new operation " << std::endl;
	std::vector<cv::Rect2d> tmpObjVec = m_trackerObjRects;
	std::vector<bool> tmpRectValidVec = m_isTrackedRectValid;
	std::vector<bool> tmpRectAddedToCount = m_isTrackedRectAddedToCount;
	m_isTrackedRectValid.clear();
	m_isTrackedRectAddedToCount.clear();
	m_trackerObjRects.clear();
	//return;
	int countOfComparedRects;
	for (int i = 0; i < tmpObjVec.size(); ++i) {
		countOfComparedRects = -1;
		for (int j = 0; j < remRectVec.size(); ++j) {
			if (remRectVec[j] == tmpObjVec[i]) {
				std::cout << "rect for remove Artash!!!!!" << std::endl;
				break;
			//	m_trackerObjRects.push_back(tmpObjVec[i]);
			//	m_strawMultiTracker->add(m_currentImg, tmpObjVec[i]);
			}
			else {
				++countOfComparedRects;
				
			}
		}
		if (countOfComparedRects == remRectVec.size() - 1) {
			m_trackerObjRects.push_back(tmpObjVec[i]);
			m_isTrackedRectValid.push_back(tmpRectValidVec[i]);
			m_isTrackedRectAddedToCount.push_back(tmpRectAddedToCount[i]);
			m_strawMultiTracker->add(m_currentImg, tmpObjVec[i]);
		}

	}
	
	std::cout << "After all operations remove" << std::endl;
	
}

void StrawberryTracker::setCurrentImage(const cv::Mat& img)
{
	m_currentImg = img.clone();
}

void StrawberryTracker::updateTrackerInfo(const cv::Mat& updImg)
{
	std::cout << "Update tracker " << std::endl;
	m_currentImg = updImg.clone();
	std::cout <<"Size of tracked rectangles "<< m_strawMultiTracker->objects.size()<<std::endl;
	m_strawMultiTracker->update(m_currentImg);
	for (int i = 0; i < m_strawMultiTracker->objects.size(); ++i) {
		m_trackerObjRects[i] = m_strawMultiTracker->objects[i];
	}

}

std::vector<cv::Rect2d> StrawberryTracker::getTrackedStrawberries()
{
	std::vector<cv::Rect2d> resTrackedStrawVec;
	//std::cout << "Before  getting size vectors  " << std::endl;
	for (int i = 0; i < m_strawMultiTracker->objects.size(); ++i) {
	//	std::cout << "Inside loop " << std::endl;
		resTrackedStrawVec.push_back(cv::Rect(m_strawMultiTracker->objects[i]));
	}
	//std::cout << "After getting size vectors  " << std::endl;
	return resTrackedStrawVec;
}

std::vector<cv::Rect2d> StrawberryTracker::getRectsFromTrackVector()
{
	std::vector<cv::Rect2d> resVec;
	for (int i = 0; i < m_trackerObjRects.size(); ++i) {
		//	std::cout << "Inside loop " << std::endl;
		resVec.push_back((m_trackerObjRects[i]));
	}
	//std::cout << "After getting size vectors  " << std::endl;
	return resVec;
}


bool StrawberryTracker::getTrackerCurrentRectValidityByIndex(const unsigned int& index) const
{
	if (m_isTrackedRectValid.size() != 0) {
		return m_isTrackedRectValid[index];
	}
	return false;
}


bool  StrawberryTracker::getTrackerCurrentRectStateForCounted(const unsigned int& index) const
{
	if (m_isTrackedRectAddedToCount.size() != 0) {
		return m_isTrackedRectAddedToCount[index];
	}
	return false;
}

cv::Rect2d StrawberryTracker::getTrackerCurrentRectByIndex(const unsigned int& index)
{
	if (m_strawMultiTracker->objects.size() != 0) {
		return m_strawMultiTracker->objects[index];
	}
}

StrawberryTracker::~StrawberryTracker()
{
	delete m_strawMultiTracker;
}
