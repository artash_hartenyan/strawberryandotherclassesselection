/****************************************************************************
** Meta object code from reading C++ file 'VideoShowDemo.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../VideoShowDemo.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VideoShowDemo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_VideoShowDemo_t {
    QByteArrayData data[14];
    char stringdata0[240];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VideoShowDemo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VideoShowDemo_t qt_meta_stringdata_VideoShowDemo = {
    {
QT_MOC_LITERAL(0, 0, 13), // "VideoShowDemo"
QT_MOC_LITERAL(1, 14, 16), // "testVideoCapture"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 22), // "onPushButtonStart_Stop"
QT_MOC_LITERAL(4, 55, 21), // "onPushbuttonOpenvideo"
QT_MOC_LITERAL(5, 77, 21), // "onPushButtonSaveImage"
QT_MOC_LITERAL(6, 99, 18), // "onChangeVideoSpeed"
QT_MOC_LITERAL(7, 118, 8), // "newValue"
QT_MOC_LITERAL(8, 127, 31), // "onChangeIntersectionAreaPercent"
QT_MOC_LITERAL(9, 159, 11), // "intersecPer"
QT_MOC_LITERAL(10, 171, 24), // "onChangeMinimumStrawSize"
QT_MOC_LITERAL(11, 196, 16), // "minimumStrawSize"
QT_MOC_LITERAL(12, 213, 17), // "onRecentOpenFiles"
QT_MOC_LITERAL(13, 231, 8) // "QAction*"

    },
    "VideoShowDemo\0testVideoCapture\0\0"
    "onPushButtonStart_Stop\0onPushbuttonOpenvideo\0"
    "onPushButtonSaveImage\0onChangeVideoSpeed\0"
    "newValue\0onChangeIntersectionAreaPercent\0"
    "intersecPer\0onChangeMinimumStrawSize\0"
    "minimumStrawSize\0onRecentOpenFiles\0"
    "QAction*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VideoShowDemo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    1,   58,    2, 0x0a /* Public */,
       8,    1,   61,    2, 0x0a /* Public */,
      10,    1,   64,    2, 0x0a /* Public */,
      12,    1,   67,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, 0x80000000 | 13,    2,

       0        // eod
};

void VideoShowDemo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VideoShowDemo *_t = static_cast<VideoShowDemo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->testVideoCapture(); break;
        case 1: _t->onPushButtonStart_Stop(); break;
        case 2: _t->onPushbuttonOpenvideo(); break;
        case 3: _t->onPushButtonSaveImage(); break;
        case 4: _t->onChangeVideoSpeed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onChangeIntersectionAreaPercent((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->onChangeMinimumStrawSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->onRecentOpenFiles((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

const QMetaObject VideoShowDemo::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VideoShowDemo.data,
      qt_meta_data_VideoShowDemo,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VideoShowDemo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VideoShowDemo::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VideoShowDemo.stringdata0))
        return static_cast<void*>(const_cast< VideoShowDemo*>(this));
    return QWidget::qt_metacast(_clname);
}

int VideoShowDemo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
