#ifndef DialogInfoi_H
#define DialogInfoi_H

#include <QDialog>


typedef std::pair<std::string, std::vector<std::string>> classAndSubClassesType;
class QLineEdit;
class QPushButton;

class DialogInfoi : public QDialog
{
    Q_OBJECT

public:
    DialogInfoi(QWidget *parent = 0);
	void clearVectorInfo();
	void clearContent();
	void setStartValue(bool);

public slots:
    void onButtonCklicked();
	void onButtonSaveClicked();
private:
    void createMembers();
    void setupLayout();
    void makeConnections();

signals:
    void onNames(QString,QString);
	void onVectorElements(classAndSubClassesType);
	


private:
    QLineEdit* m_className;
    QLineEdit* m_subName;
    QPushButton* m_add;
	QPushButton* m_saveCurrentClass;
	classAndSubClassesType m_classWithSubClassesVec;
    bool isstart;


};

#endif // DialogInfoi_H
